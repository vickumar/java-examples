package examples;

import models.Animal;
import java.util.List;

public interface TypeVarianceWithClassInfoExample<T extends Animal> {
    default void makeSounds(List<T> animals, Class<T> classTag) {
        for (T a: animals) {
            System.out.println(a.getName() + ": " + a.speak());
        }
        System.out.println("\n" + animals.getClass().getName() + " of " + classTag.getName() + "\n");
    }
}
