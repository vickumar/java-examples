package examples;

import models.Animal;
import models.Cat;
import models.Dog;

import java.util.ArrayList;
import java.util.List;

public interface TypeVarianceExample {
    default void testSingleType() {
        Dog dog1 = new Dog("Spot");
        makeSound(dog1);
        Cat cat1 = new Cat("Kitty");
        makeSound(cat1);
    }

    private void makeSound(Animal animal) {
        System.out.println(animal.getName() + ": " + animal.speak());
        System.out.println("\n" + animal.getClass().getName() + "\n");
    }

    default void testParameterTypeVarianceArrays() {
        Dog [] dogs = new Dog[3];
        dogs[0] = new Dog("Toto");
        dogs[1] = new Dog("Sparky");
        dogs[2] = new Dog("I'm a dog");
        makeSounds(dogs);
    }

    private void makeSounds(Animal [] animals) {
        //animals[0] = new Cat("I'm a cat, not a dog");
        for (Animal a: animals) {
            System.out.println(a.getName() + ": " + a.speak());
        }
        System.out.println("\n" + animals.getClass().getName() + "\n");
    }

    default void testParameterTypeVarianceLists() {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Dog("Toto"));
        animals.add(new Dog("Sparky"));
        animals.add(new Dog("I'm a dog"));
        animals.add(new Cat("I'm a cat."));
        makeSounds2(animals);
    }

    private void makeSounds2(List<? extends Animal> animals) {
//        animals.add(
//                new Cat("Why is this allowed?")
//        );
        for (Animal a: animals) {
            System.out.println(a.getName() + ": " + a.speak());
        }
        System.out.println("\n" + animals.getClass().getName() + "\n");
    }
}
