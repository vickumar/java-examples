package examples;

import models.ChildClass;
import models.ParentClass;

public interface AccessModifierExample {

    default void testAccessModifiers() {
        // var childClass = new ParentClass() // Cannot do b/c ParentClass is abstract
        var childClass = new ChildClass();
        System.out.println("Calling public method in parent class...");
        childClass.pubicGreeting();

        System.out.println("Calling method in child class...");
        childClass.greeting();
    }
}
