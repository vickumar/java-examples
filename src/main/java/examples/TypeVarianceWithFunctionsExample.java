package examples;

import models.Animal;
import models.Cat;
import models.Dog;
import models.Poodle;
import models.Siamese;

import java.util.function.Function;

public interface TypeVarianceWithFunctionsExample {
    default void testVarianceWithFunctions() {
        Function<Animal, Cat> func1 = a -> new Siamese("Siamese cat");
        Function<Poodle, Cat> func2 = p -> new Siamese("Siamese cat #2");

        System.out.println(needsDog(func1, new Poodle("Poodle 1")).meow());
//        System.out.println(needsDog(func2, new Poodle("Poodle")));

    }


    private Cat needsDog(Function<? super Dog, ? extends Cat> func, Dog a) {
        System.out.println("Applying input parameter " + a.getName());
        return func.apply(a);
    }
}
