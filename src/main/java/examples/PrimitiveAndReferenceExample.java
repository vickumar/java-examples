package examples;

public abstract class PrimitiveAndReferenceExample {
    private int primitiveInt;
    private Integer referenceInt;

    private boolean primitiveBool;
    private Boolean referenceBool;

    protected void primativeVsReferenceExample() {
        System.out.println("int: " + primitiveInt
                + "\nInteger: " + referenceInt
                + "\nboolean: " + primitiveBool
                + "\nBoolean: " + referenceBool);
    }
}