
import examples.*;
import models.*;

import java.util.ArrayList;

public class App extends PrimitiveAndReferenceExample
        implements AccessModifierExample, TypeVarianceExample,
        TypeVarianceWithClassInfoExample<Dog>, TypeVarianceWithFunctionsExample {
    private App() {}

    public static void main(String[] args) {
        var app = new App();
        // app.primativeVsReferenceExample();
        // app.testAccessModifiers();
        // app.testSingleType();
        // app.testParameterTypeVarianceArrays();
        // app.testParameterTypeVarianceLists();
        // app.testParameterTypeVarianceWithClassInfo();
        // app.testVarianceWithFunctions();
    }

    private void testParameterTypeVarianceWithClassInfo() {
        var animals = new ArrayList<Dog>();
        animals.add(new Dog("Toto"));
        animals.add(new Dog("Sparky"));
        animals.add(new Dog("I'm a dog"));
        makeSounds(animals, Dog.class);
    }
}
