package models;

public interface Animal {
    String getName();
    String speak();
}
