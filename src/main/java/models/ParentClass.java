package models;

public abstract class ParentClass {
    private void privateGreeting() {
        System.out.println("Hi.  I am the private greeting.");
    }

    protected void protectedGreeting() {
        privateGreeting();
        System.out.println("Hi.  I am the protected greeting.");
    }

    public void pubicGreeting() {
        privateGreeting();
        System.out.println("Hi.  I am the public greeting.");
    }
}
