package models;

public class Cat implements Animal {
    private String name;
    public Cat(String name) { this.name = name; }

    @Override
    public String speak() {
        return "Meow";
    }

    @Override
    public String getName() { return name; }

    public String meow() {
        return "I'm a cat";
    }
}
