package models;

public class Dog implements Animal {
    private String name;
    public Dog(String name) { this.name = name; }

    @Override
    public String speak() {
        return "Woof";
    }

    @Override
    public String getName() { return name; }
}
